/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetablefactory;


public class VegetableDemo {
    public static void main(String args[]){
        VegetableFactory factory = VegetableFactory.getInstance();
        
        Vegetable carrot = factory.getVegetable("orange", 2);
         Vegetable beet = factory.getVegetable("red", 4);
        
        System.out.println("Colour of " + carrot.getClass().getCanonicalName() + " is " + carrot.getColor() + 
                 " and is it ripe? " + carrot.isRipe());
                
               
        System.out.println("Colour of " + beet.getClass().getCanonicalName() + " is " + beet.getColor() + 
                 " and is it ripe? " + beet.isRipe());
                
        
        
        
        
    }
}
