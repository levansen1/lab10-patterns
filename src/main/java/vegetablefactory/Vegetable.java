/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetablefactory;

/**
 *
 * @author levansen
 */

public abstract class Vegetable {
    protected String color;
    protected double size;
    
     Vegetable(){

}
     Vegetable(String color, double size){
         this.color = color;
         this.size = size;
     }

 
    
        public abstract String getColor();

    public abstract void setColor(String color);

    public abstract double getSize();

    public abstract void setSize(double size);
    
    public abstract boolean isRipe();
     
   
    
    
    
    
}
