/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetablefactory;


public class Carrot extends Vegetable {
    
    Carrot(){
        
    }
    
     Carrot(String color, double size){
        this.color = color;
        this.size = size;
    }
    
     public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
    
    public  boolean isRipe(){
        if(size>1.5 && color=="orange"){
            return true;
        }
        else return false;
    }
     
}
