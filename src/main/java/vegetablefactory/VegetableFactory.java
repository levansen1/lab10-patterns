/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vegetablefactory;

public class VegetableFactory {
    
    private static VegetableFactory obj;
    
    private VegetableFactory(){
        
    }
    
    public static VegetableFactory getInstance(){
        if(obj == null){
            obj = new VegetableFactory();
        } 
        return obj;
       
    }
    
    public Vegetable getVegetable(String color, double size){
        if(color == "orange" && size>1.5){
            return new Carrot(color,size);
        }
        else if(color == "red" && size>2){
            return new Beet("red",size);
        }
        return null;
        
    }
    
    
    
}
